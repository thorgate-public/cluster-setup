# Ansible Collection - thorgate-public.ansible_shared

Shared ansible roles we use at Thorgate. They are mainly used to avoid code duplication.

## Using the collection

**Install it:**

1. Add the following into a `requirements.yml` file in your ansible repository. If the file does not exist create it.
  ```
  collections:
    - name: git@gitlab.com:thorgate-public/tg-cluster-management.git
      type: git
      version: v1.0.0
  ```
2. Run the following command to install the collection: `ansible-galaxy collection install -r requirements.yml`
3. And then you can use it in your playbook:

```
- include_role: name=cluster-info
  collections:
    - thorgate_public.cluster_management
```


**Note:** For some reason the FQDN way of importing tasks does not work. Roles should be imported using
the collection keyword. For example, the script above works, while this does not:

```
- include_role: name=thorgate_public.cluster_management.cluster-info  # does not work for some reason
```

## License

BSD

## Author Information

This role is created and used by [Thorgate](https://thorgate.eu).
