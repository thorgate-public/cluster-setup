prometheus:
  thanosService:
    enabled: true

  prometheusSpec:
    replicas: 1
    retention: 1d
    disableCompaction: false
    serviceMonitorNamespaceSelector:
      any: true  # allows the operator to find target config from multiple namespaces
    storageSpec:
      volumeClaimTemplate:
        spec:
          accessModes: [ "ReadWriteOnce" ]
          resources:
            requests:
              storage: "{{ prometheus_pv_size }}"
          storageClassName: "{{ storage_class }}"
    thanos:
      objectStorageConfig:
        key: thanos.yaml
        name: thanos-objstore-config

  ingress:
    enabled: true
    annotations:
      kubernetes.io/ingress.class: nginx
      nginx.ingress.kubernetes.io/whitelist-source-range: "{{ ip_address_whitelist | join(',') }}"
    hosts:
      - "prometheus.{{ cluster_base_domain }}"
    tls:
      - secretName: cluster-wildcard-cert-tls
        hosts:
          - "prometheus.{{ cluster_base_domain }}"

alertmanager:
  config:
    global:
      resolve_timeout: 5m
      slack_api_url: "{{ monitoring_slack_api_url }}"
    route:
      group_by: ['alertname', 'cluster', 'service']
      group_wait: {{ alertmanager_group_wait }}
      group_interval: {{ alertmanager_group_interval }}
      repeat_interval: {{ alertmanager_repeat_interval }}
      receiver: "{{ alertmanager_default_receiver }}"
      routes:
        # {%- for rule in alertmanager_routing_rules|default([]) %}

        - receiver: "{{ rule.receiver }}"
          matchers: {{ rule.matchers | from_yaml }}

        # {%- endfor %}

    receivers:
      - name: 'null'  # Empty receiver

      # {% for rule in alertmanager_receivers|default([]) %}

      - name: "{{ rule.name }}"
        slack_configs: {{ rule.slack_configs | from_yaml }}

      # {% endfor %}

  templateFiles:
    # {% raw %}
    slack_notification_title.tmpl: |-
      {{ define "slack.thorgate.title" }}
      [{{ .Status | toUpper }}{{ if eq .Status "firing" }}:{{ .Alerts.Firing | len }}{{ end }}] {{ .CommonLabels.alertname }} for {{ .CommonLabels.job }}
      {{- if gt (len .CommonLabels) (len .GroupLabels) -}}
        {{" "}}(
        {{- with .CommonLabels.Remove .GroupLabels.Names }}
          {{- range $index, $label := .SortedPairs -}}
            {{ if $index }}, {{ end }}
            {{- $label.Name }}="{{ $label.Value -}}"
          {{- end }}
        {{- end -}}
        )
      {{- end }}
      {{- end }}
    slack_notification_text.tmpl: |-
      {{ define "slack.thorgate.text" }}
      {{ range .Alerts -}}
      *Alert:* {{ .Annotations.title }}{{ if .Labels.severity }} - `{{ .Labels.severity }}`{{ end }}
      *Description:* {{ .Annotations.description }}
      *Details:*
        {{ range .Labels.SortedPairs }} • *{{ .Name }}:* `{{ .Value }}`
        {{ end }}
      {{ end }}
      {{ end }}
    # {% endraw %}

  ingress:
    enabled: true
    annotations:
      kubernetes.io/ingress.class: nginx
      nginx.ingress.kubernetes.io/whitelist-source-range: "{{ ip_address_whitelist | join(',') }}"
      nginx.ingress.kubernetes.io/satisfy: any
      nginx.ingress.kubernetes.io/auth-type: basic
      nginx.ingress.kubernetes.io/auth-secret: alertmanager-basic-auth
      nginx.ingress.kubernetes.io/auth-realm: 'Authentication Required - Alertmanager'
    hosts:
      - "alertmanager.{{ cluster_base_domain }}"
    tls:
      - secretName: cluster-wildcard-cert-tls
        hosts:
          - "alertmanager.{{ cluster_base_domain }}"

  alertmanagerSpec:
    storage:
      volumeClaimTemplate:
        spec:
          storageClassName: "{{ storage_class }}"
          accessModes: ["ReadWriteOnce"]
          resources:
            requests:
              storage: "{{ alertmanager_pv_size }}"


grafana:
  enabled: true
  deploymentStrategy:
    type: Recreate # Will add downtime but allows for upgrade without ReadWriteMany
  adminPassword: "{{ grafana_admin_password }}"
  sidecar:
    datasources:
      enabled: true
      label: grafana_datasource
  dashboardProviders:
    dashboardproviders.yaml:
      apiVersion: 1
      providers:
        - name: default
          orgId: 1
          folder:
          type: file
          disableDeletion: true
          editable: false
          options:
            path: /var/lib/grafana/dashboards/default
  dashboards:
    default:
      node-exporter-full:
        gnetId: 1860
        revision: 14
        datasource: Prometheus
      nginx-ingress-controller:
        gnetId: 9614
        revision: 1
        datasource: Prometheus

  # Enable persistence to be able to give access to users
  persistence:
    enabled: true
    storageClassName: "{{ storage_class }}"


# - {% if cluster_type == 'internal' %}

  additionalDataSources:
    - name: Thanos
      type: prometheus
      url: http://thanos-query.cluster-monitoring.svc.cluster.local:9090
      access: proxy
      isDefault: false

# - {% endif %}

  grafana.ini:
    server:
      root_url: "https://grafana.{{ cluster_base_domain }}"

    users:
      verify_email_enabled: true

    smtp:
      enabled: true
      host: "{{ smtp_host }}:{{ smtp_port }}"
      from_address: "grafana@{{ base_from_email }}"

    auth.google:
      enabled: true
      client_id: "{{ grafana_google_client_id }}"
      client_secret: "{{ grafana_google_client_secret }}"
      scopes: "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email"
      auth_url: "https://accounts.google.com/o/oauth2/auth"
      token_url: "https://accounts.google.com/o/oauth2/token"
      allowed_domains: "thorgate.eu"
      allow_sign_up: true

  smtp:
    existingSecret: "grafana-email-secrets"
    userKey: "user"
    passwordKey: "password"

  ingress:
    enabled: true
    annotations:
      kubernetes.io/ingress.class: nginx
      nginx.ingress.kubernetes.io/whitelist-source-range: "{{ ip_address_whitelist | join(',') }}"
    hosts:
    - "grafana.{{ cluster_base_domain }}"
    tls:
    - secretName: cluster-wildcard-cert-tls
      hosts:
      - "grafana.{{ cluster_base_domain }}"
