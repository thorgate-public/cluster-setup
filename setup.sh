#!/usr/bin/env bash

# Load required directories to env for other scripts
DIR="$(cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)"

export TG_CLUSTER_SETUP_DIR="${DIR}"
export TG_CLUSTER_SETUP_SCRIPTS_DIR="${DIR}/scripts"
export PATH="${PATH}:${TG_CLUSTER_SETUP_SCRIPTS_DIR}/utils"

# Load helpers for scripts
# shellcheck disable=SC1090
source <(cat "${TG_CLUSTER_SETUP_SCRIPTS_DIR}"/*.sh)
