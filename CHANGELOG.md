# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [1.0.0] - 2022-12-02

This is the first version of Thorgate's shared ansible collection. The first version contains
the following roles:

* [cluster-info](./roles/cluster-info)
* [fluentd-agent](./roles/fluentd-agent)
* [gitlab-integration](./roles/gitlab-integration)
* [gitlab-runner](./roles/gitlab-runner)
* [graylog](./roles/graylog)
* [ingress](./roles/ingress)
* [prometheus-operator](./roles/prometheus-operator)
* [thanos](./roles/thanos)
