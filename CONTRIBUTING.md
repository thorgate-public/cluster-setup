<!-- omit in toc -->
# Contributing to thorgate_public.cluster_management

First off, thanks for taking the time to contribute! ❤️

All types of contributions are encouraged and valued. See the [Table of Contents](#table-of-contents) for different ways to help and details about how this project handles them. Please make sure to read the relevant section before making your contribution. It will make it a lot easier for us maintainers and smooth out the experience for all involved. We look forward to your contributions. 🎉

<!-- omit in toc -->
## Table of Contents

- [I Have a Question](#i-have-a-question)
- [I Want To Contribute](#i-want-to-contribute)
  - [Reporting Bugs](#reporting-bugs)
  - [Suggesting Enhancements](#suggesting-enhancements)
  - [Your First Code Contribution](#your-first-code-contribution)
  - [Testing roles](#testing-roles)
  - [Releasing a new version](#releasing-a-new-version)



## I Have a Question

Before you ask a question, it is best to search for existing [Issues](https://gitlab.com/thorgate-public/tg-cluster-management/issues) that might help you. In case you have found a suitable issue and still need clarification, you can write your question in this issue. It is also advisable to search the internet for answers first.

If you then still feel the need to ask a question and need clarification, we recommend the following:

- Open an [Issue](https://gitlab.com/thorgate-public/tg-cluster-management/issues/new).
- Provide as much context as you can about what you're running into.
- Provide project and platform versions (python, ansible, poetry/pip, etc), depending on what seems relevant.

We will then take care of the issue as soon as possible.

**Thorgate employees** can also ask questions about the project via Slack. The channel to do this in is #it.

## I Want To Contribute

> ### Legal Notice <!-- omit in toc -->
> When contributing to this project, you must agree that you have authored 100% of the content, that you have the necessary rights to the content and that the content you contribute may be provided under the project license.

### Reporting Bugs

<!-- omit in toc -->
#### Before Submitting a Bug Report

A good bug report shouldn't leave others needing to chase you up for more information. Therefore, we ask you to investigate carefully, collect information and describe the issue in detail in your report. Please complete the following steps in advance to help us fix any potential bug as fast as possible.

- Make sure that you are using the latest version.
- Determine if your bug is really a bug and not an error on your side e.g. using incompatible environment components/versions (Make sure that you have read the role documentation). If you are looking for support, you might want to check [this section](#i-have-a-question)).
- To see if other users have experienced (and potentially already solved) the same issue you are having, check if there is not already a bug report existing for your bug or error in the [bug tracker](https://gitlab.com/thorgate-public/tg-cluster-management/issues?q=label%3Abug).
- Also make sure to search the internet (including Stack Overflow) to see if users outside of the community have discussed the issue.
- Collect information about the bug:
  - Stack trace (Traceback)
  - OS, Platform and Version (Windows, Linux, macOS, x86, ARM)
  - Version of the interpreter, compiler, SDK, runtime environment, package manager, depending on what seems relevant.
  - Possibly your input and the output
  - Can you reliably reproduce the issue? And can you also reproduce it with older versions?

<!-- omit in toc -->
#### How Do I Submit a Good Bug Report?

> You must never report security related issues, vulnerabilities or bugs to the issue tracker, or elsewhere in public. Instead sensitive bugs must be sent by email to <it@thorgate.eu>.

We use Gitlab issues to track bugs and errors. If you run into an issue with the project:

- Open an [Issue](https://gitlab.com/thorgate-public/tg-cluster-management/issues/new). (Since we can't be sure at this point whether it is a bug or not, we ask you not to talk about a bug yet and not to label the issue.)
- Explain the behavior you would expect and the actual behavior.
- Please provide as much context as possible and describe the *reproduction steps* that someone else can follow to recreate the issue on their own. This usually includes your code. For good bug reports you should isolate the problem and create a reduced test case.
- Provide the information you collected in the previous section.

Once it's filed:

- The project team will label the issue accordingly.
- A team member will try to reproduce the issue with your provided steps. If there are no reproduction steps or no obvious way to reproduce the issue, the team will ask you for those steps and mark the issue as `needs-repro`. Bugs with the `needs-repro` tag will not be addressed until they are reproduced.
- If the team is able to reproduce the issue, it will be marked `needs-fix`, as well as possibly other tags (such as `critical`), and the issue will be left to be [implemented by someone](#your-first-code-contribution).


### Suggesting Enhancements

This section guides you through submitting an enhancement suggestion for tg-ansible-shared, **including completely new features and minor improvements to existing functionality**. Following these guidelines will help maintainers and the community to understand your suggestion and find related suggestions.

<!-- omit in toc -->
#### Before Submitting an Enhancement

- Make sure that you are using the latest version.
- Read the roles documentation carefully and find out if the functionality is already covered, maybe by an individual configuration.
- Perform a [search](https://gitlab.com/thorgate-public/tg-cluster-management/issues) to see if the enhancement has already been suggested. If it has, add a comment to the existing issue instead of opening a new one.
- Find out whether your idea fits with the scope and aims of the project. It's up to you to make a strong case to convince the project's developers of the merits of this feature. Keep in mind that we want features that will be useful to the majority of our users and not just a small subset. If you're just targeting a minority of users, consider writing an add-on/plugin library.

<!-- omit in toc -->
#### How Do I Submit a Good Enhancement Suggestion?

**Thorgate employees** can also suggest enhancements via Slack. The channel for this is #devops-messageboard.

Enhancement suggestions are tracked as [GitHub issues](https://gitlab.com/thorgate-public/tg-cluster-management/issues).

- Use a **clear and descriptive title** for the issue to identify the suggestion.
- Provide a **step-by-step description of the suggested enhancement** in as many details as possible.
- **Describe the current behavior** and **explain which behavior you expected to see instead** and why. At this point you can also tell which alternatives do not work for you.
- You may want to **include screenshots and animated GIFs** which help you demonstrate the steps or point out the part which the suggestion is related to.
- **Explain why this enhancement would be useful** to most tg-ansible-shared users. You may also want to point out the other projects that solved it better and which could serve as inspiration.

### Your First Code Contribution
<!-- TODO
include Setup of env, IDE and typical getting started instructions?

-->
<!-- 

TODO

### Creating Your Own Role

TODO -->

### Testing roles

Tests can be written with either:

- [Testinfra](https://testinfra.readthedocs.io/en/latest/index.html)
- [Ansible asserts](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/assert_module.html)

See existing test for examples or alternatively look into Ansible's blog post on the topic:

- https://www.ansible.com/blog/developing-and-testing-ansible-roles-with-molecule-and-podman-part-1
- https://docs.ansible.com/ansible/latest/dev_guide/developing_collections_testing.html
- https://molecule.readthedocs.io/en/latest/

Note: To make tests work on the *development* version of the collection not the released and installed one
make sure to run `rm -rf ~/.ansible/collections` before running the tests.

<!-- TODO
### Improving The Documentation

Updating, improving and correcting the documentation

## Styleguides

-->


### Releasing a new version

To release a new version of the package you should first update the CHANGELOG. The first step is to add
a date and a version number header right after the  `## Unreleased` line. This essentially creates a new
unreleased section and moves everything under the development version into the release. As a sanity check
it also makes sense to go over the changelog to make sure nothing is missing from it.

After this you should update the version number in the following files:

- [galaxy.yml](./galaxy.yml)
- [README.md](./README.md)

And after this commit your changes with git:

```bash
# Make sure to use the correct version number
git commit -pm 'Release version v0.0.0'
```

After this you should push the changes and then wait for the CI to pass. Once CI passes you can create a tag
for the release and push it:

```bash
# Make sure to use the correct version number
git tag -a v0.0.0 -m 'Version 0.0.0'
git push origin --tags
```

Finally create the release in gitlab - https://gitlab.com/thorgate-public/tg-cluster-management/-/releases/new
Make sure to select the existing tag name and add the release notes from changelog file.

After this the new version is available and can be imported to other ansible playbooks. Be sure to let everyone
know about it in #it channel.
